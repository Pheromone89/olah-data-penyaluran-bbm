from datetime import datetime
from ensurepip import version
import gc
import os
from platform import platform
import sys
from typing import overload
from numpy import TooHardError, absolute, float64
import pandas as pd
import glob
import math

from requests import request

# mas muhamad dzulfikar
# biro SDM BPKP

program_version = 4.0

def convert_size(size_bytes):
   if size_bytes == 0:
       return "0B"
   size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
   i = int(math.floor(math.log(size_bytes, 1024)))
   p = math.pow(1024, i)
   s = round(size_bytes / p, 2)
   return "%s %s" % (s, size_name[i])

def convert_time(seconds):
    seconds = seconds % (24*3600)
    hour = seconds // 3600
    seconds %= 3600
    minutes = seconds // 60
    seconds %= 60

    return '%d:%02d:%02d'%(hour, minutes, seconds)

def all_letter(value):
    for character in value:
        if character.isdigit():
            return False
    return True

# MODE
print_aggregate_transaction_details = True
print_transaction_with_no_nopol = True
print_transaction_under_one_liter = True
print_transaction_over_two_hundred_liter = True
print_transaction_excluded = True

now = datetime.now()

file_size_in_bytes = 0

total_rupiah = 0.0
total_liter = 0.0
treshold = 200
min_amount = 1

total_rupiah_tanpa_nopol = 0.0
total_liter_tanpa_nopol = 0.0
total_transaksi = 0
total_rows = 0
success_logs: list[str] = []
failed_files_list: list[str] = []
error_log: list[str] = []
failed_rows = 0
over_list_details: dict = {}
no_nopol_list: list[dict] = []
under_transactions: list[dict] = []
excluded_transactions: list[dict] = []

unique_day_car: dict = {}

cols_list = ['hose_number','pump_name','attendant_name','delivery_id','site_id','city','product','completed_date','TIME','delivery_type','delivery_volume','delivery_value','vehicle_number']
defined_mor_list = ["dummy", "MOR-1","MOR-2","MOR-3","MOR-4","MOR-5","MOR-6","MOR-7","MOR-8"]

keywords = ['polri', 'polda', 'polsek', 'kapolsek', 'polisi', 'tni', 'marinir', 'kantor', 'polres', 'merah', 'dinas', 'skpd', 'patwal']

picked_mor = []
def request_input():
    try:
        user_input = input('masukkan mor yang ingin Anda olah. Pisahkan dengan koma <,>. contoh: \'1,2,3\' dst: ').split(',')
        if user_input == ['a']:
            return ['1', '2', '3', '4','5', '6', '7', '8']
        if not isinstance(user_input, list):
            print('Input tidak sesuai')
            return request_input()
        if len(user_input) == 0:
            print('Input request terlebih dahulu')
            return request_input()
        else:
            try:
                for item in user_input:
                    index = int(item)
                    
                return user_input
            except:
                print('Input bukan nomor')
                return request_input()
    except:
        print('Input tidak valid')
        return request_input()

def request_aggregate():
    user_input = input('Apakah Anda ingin mencetak rincian transaksi > 200L beserta agregat nya?(y/n) ')
    if user_input == 'y':
        return True
    if user_input == 'n':
        return False
    print('Jawaban tidak valid')
    request_aggregate()

def request_no_nopol():
    user_input = input('Apakah Anda ingin mencetak daftar transaksi tanpa nomor polisi?(y/n) ')
    if user_input == 'y':
        return True
    if user_input == 'n':
        return False
    print('Jawaban tidak valid')
    request_no_nopol()

def request_under():
    user_input = input('Apakah Anda ingin mencetak daftar transaksi di bawah 1 liter?(y/n) ')
    if user_input == 'y':
        return True
    if user_input == 'n':
        return False
    print('Jawaban tidak valid')
    request_under()

def request_over():
    user_input = input('Apakah Anda ingin mencetak rekap transaksi di atas 200 liter (per nopol per hari)?(y/n) ')
    if user_input == 'y':
        return True
    if user_input == 'n':
        return False
    print('Jawaban tidak valid')
    request_over()

def request_excluded():
    user_input = input('Apakah Anda ingin mencetak daftar transaksi yang dikecualikan?(y/n) ')
    if user_input == 'y':
        return True
    if user_input == 'n':
        return False
    print('Jawaban tidak valid')
    request_excluded()

picked_mor = request_input()
print_aggregate_transaction_details = request_aggregate()
print_transaction_with_no_nopol = request_no_nopol()
print_transaction_under_one_liter = request_under()
print_transaction_over_two_hundred_liter = request_over()
print_transaction_excluded = request_excluded()

if print_transaction_excluded:
    print('Kata kunci default:\n' + str(keywords) + '\nKetik kata kunci tambahan yang ingin Anda include. Pisahkan dengan koma <,>. Enter jika sudah selesai / ingin melewati langkah ini')
    additional_keyword = input('').split(',')
    for keyword in additional_keyword:
        key = str.strip(keyword.lower())
        if key != '':
            keywords.append(key)
# print(str(keywords))

previous_mor = ''
mor_list = []
for index in picked_mor:
    mor_list.append(defined_mor_list[int(index)])

for index, folder_title in enumerate(mor_list):
    all_files = glob.glob("data/" + folder_title + "/*.csv")
    if len(all_files) == 0:
        print(" no file in " + folder_title)
    file_number = 0
    for filename in all_files:
        try:
            start = datetime.now()
            separator = ","
            with open(filename) as f:
                separator_index = 0
                line = f.readline().rstrip()
                for char_index, char in enumerate(line):
                    if char == ";" or char == ",":
                        separator_index = char_index
                        break
                separator = line[separator_index]

            file_number += 1
            item = pd.read_csv(filename,sep=separator, usecols=cols_list)
            item.insert(0, 'mor', folder_title)
            print("\r", "adding from " + folder_title + ": " + str(file_number) + "/" + str(len(all_files)) + " : " + filename + "                       ", end="")
            file_size_in_bytes += os.path.getsize(filename)

            size = str(len(item))

            print("")
            for file_index, row in item.iterrows():
                try:
                    total_rows += len(item)
                    percentage = '{:,.2f}%'.format(((file_index + 1) / len(item)) * 100.0)
                    print("\r", "parsing " + folder_title + " " + str(file_number) + "/" + str(len(all_files)) + " - " + filename + " : " + percentage + "                       ", end="")

                    vehicle_number = str(row['vehicle_number'])
                    attendant_name = str(row.to_dict()['attendant_name'])

                    cleaned_vehicle_number = str.strip(vehicle_number).replace('nan', '')
                    cleaned_attendant_name = attendant_name.replace('nan', '')

                    total_rupiah = total_rupiah + row['delivery_value']
                    total_liter = total_liter + row['delivery_volume']
                    total_transaksi += 1

                    if print_transaction_under_one_liter:
                        if row['delivery_volume'] < min_amount:
                            under_transactions.append(row.to_dict())
                    
                    if (not cleaned_vehicle_number or cleaned_vehicle_number == "" or cleaned_vehicle_number == "nan") and print_transaction_with_no_nopol:
                        no_nopol_list.append(row.to_dict())
                    else:
                        if print_transaction_excluded:
                            if cleaned_vehicle_number.lower() in keywords:
                                excluded_transactions.append(row.to_dict())
                        # create unique dict
                        key = row.to_dict()['completed_date'] + "|" + cleaned_vehicle_number + "|" + row.to_dict()['product']

                        transaction_dict = {
                            'delivery_volume': row.to_dict()['delivery_volume'],
                            'aggregate_volume': row.to_dict()['delivery_volume'],
                            'delivery_value': row.to_dict()['delivery_value'],
                            'attendant_name': cleaned_attendant_name,
                            'delivery_type': row.to_dict()['delivery_type'],
                            'TIME': row.to_dict()['TIME'],
                            'city': row.to_dict()['city'],
                            'site_id': row.to_dict()['site_id'],
                            'hose_number': row.to_dict()['hose_number'],
                            'pump_name': row.to_dict()['pump_name'],
                        }

                        try:
                            unique_day_car[key]['total_volume'] += transaction_dict['delivery_volume']
                            if print_aggregate_transaction_details:
                                transaction_dict['aggregate_volume'] = unique_day_car[key]['total_volume']
                                unique_day_car[key]['transactions'] += [transaction_dict]
                            else:
                                unique_day_car[key]['count'] += 1
                        except KeyError:
                            unique_day_car[key] = {
                                'mor': folder_title,
                                'total_volume': transaction_dict['delivery_volume'],
                                # 'count': 1,
                                # 'transactions': [transaction_dict]
                            }
                            if print_aggregate_transaction_details:
                                unique_day_car[key]['transactions'] = [transaction_dict]
                            else:
                                unique_day_car[key]['count'] = 1

                except:
                    if index == 0:
                        failed_files_list.append(filename)
                        break
                    else:
                        failed_rows += 1

            time_gap = convert_time((datetime.now() - start).seconds)
            print("\nfinished in: " + time_gap + "\n", end="")
            success_logs.append(filename + " in " + time_gap)
            
            del item
            gc.collect()
        except:
            failed_files_list.append(filename)
            error_log.append(str(sys.exc_info()) + " occured at: " + filename)
            print('\nunable to parse CSV from: ' + filename)
    previous_mor = folder_title

if print_aggregate_transaction_details or print_transaction_over_two_hundred_liter:
    print("\nfinished parsing")
    current_item = 0
    size = len(unique_day_car.keys())
    for key in unique_day_car.keys():
        current_item += 1
        percentage = '{:,.2f}%'.format((current_item / size) * 100.0)
        print("\r", "checking over limit " + percentage + ' with key ' + key + '                       ', end="")

        volume = unique_day_car[key]['total_volume']
        if volume > treshold:
            over_list_details[key] = unique_day_car[key]

    del unique_day_car
    print("\nfinished checking")

total_file_size_string = convert_size(file_size_in_bytes)
total_rupiah_string = '{:,.0f}'.format(total_rupiah)
total_liter_string = '{:,.2f}'.format(total_liter)
total_transaksi_string = '{:,.0f}'.format(total_transaksi)
total_transaksi_tanpa_nopol_string = '{:,.0f}'.format(len(no_nopol_list))
total_transaksi_under_string = '{:,.0f}'.format(len(under_transactions))
over_transaction_string = '{:,.0f}'.format(len(over_list_details))
excluded_string = '{:,.0f}'.format(len(excluded_transactions))

finished_time = datetime.now() 
elapsed_time = finished_time - now

separator = '--------------------------------------------------------'
empty_line = "\n"

summary = ""

summary += "\n" + separator
summary += empty_line
summary += "\n" + 'program version: ' + str(program_version)
summary += "\n" + 'finished in: ' + convert_time(elapsed_time.seconds)
summary += "\n" + 'total file size: ' + total_file_size_string
summary += "\n" + 'total files: ' + str(len(success_logs) + len(failed_files_list)) + ' files'
summary += "\n" + 'successful files analyzed: ' + str(len(success_logs))
summary += "\n" + 'failed files analyzed: ' + str(len(failed_files_list))
summary += "\n" + 'failed rows analyzed: ' + str(failed_rows)
summary += empty_line
summary += "\n" + separator
summary += empty_line
summary += "\n" + 'mode:'
if print_aggregate_transaction_details:
    summary += "\n" + 'Melakukan analisis volume per transaksi beserta agregatnya per nopol per hari'
if print_transaction_with_no_nopol:
    summary += "\n" + 'Melakukan analisis rincian transaksi tanpa nomor polisi'
if print_transaction_under_one_liter:
    summary += "\n" + 'Melakukan analisis transaksi di bawah 1 liter'
if print_transaction_over_two_hundred_liter:
    summary += "\n" + 'Melakukan analisis transaksi di atas 200 liter'
if excluded_transactions:
    summary += "\n" + 'Melakukan analisis transaksi pengecualian'
summary += empty_line
summary += "\n" + separator
summary += empty_line
summary += "\n" + 'total nominal: Rp' + total_rupiah_string
summary += "\n" + 'total volume: ' + total_liter_string + ' liter'
summary += empty_line
summary += "\n" + separator
summary += empty_line
summary += "\n" + 'total transaksi: ' + str(total_transaksi_string)
if print_transaction_with_no_nopol:
    summary += "\n" + 'total transaksi tanpa nopol: ' + str(total_transaksi_tanpa_nopol_string)
if print_transaction_under_one_liter:
    summary += "\n" + 'total transaksi di bawah 1L: ' + str(total_transaksi_under_string)
if print_transaction_over_two_hundred_liter:
    summary += "\n" + 'total transaksi melebihi 200L per nopol per hari: ' + over_transaction_string
if excluded_transactions:
    summary += "\n" + 'total transaksi dengan plat nomor berdasarkan kata kunci\n' + str(keywords) + '\ntotal: ' + excluded_string
summary += empty_line
summary += "\n" + separator
summary += empty_line

time_label = finished_time.strftime("%Y-%m-%d %H-%M-%S")
folder_title = 'MOR-' + ', '.join(picked_mor)
folder_name = 'report\\' + time_label + ' - ' + folder_title + '\\'

dir1 = os.path.join(os.getcwd(), folder_name)
try:
    if not os.path.exists('report'):
        # create report folder
        os.mkdir('report')
    if not os.path.exists(folder_name):
        # create time sub folder
        os.mkdir(folder_name)
except:
    error_log(str(sys.exc_info()))

if print_transaction_over_two_hundred_liter:
    with open(dir1 + "over_limit_summary.csv", "w") as txtfile:
        txtfile.write("No|Plat Nomor|Tanggal|Jam|MOR|Produk|Volume|Kelebihan Kuota|Jumlah Transaksi")
        for index, line in enumerate(over_list_details):
            gap = treshold - over_list_details[line]['total_volume']

            identifier = line.split('|')
            date = identifier[0]
            plat = identifier[1]
            product = identifier[2]

            count = 0
            time = ''
            if print_aggregate_transaction_details:
                count = len(list(over_list_details[line]['transactions']))
                time = over_list_details[line]['transactions'][0]['TIME']
            else:
                count = over_list_details[line]['count']

            txtfile.write(
                "\n" + str(index + 1)
                + "|" + plat
                + "|" + date
                + "|" + time
                + "|" + over_list_details[line]['mor']
                + "|" + product
                + "|" + '{:,.2f}'.format(over_list_details[line]['total_volume'])
                + "|" + '{:,.2f}'.format(abs(gap))
                + "|" + str(count)
                )

if print_aggregate_transaction_details:
    with open(dir1 + "over_limit_transactions.csv", "w") as txtfile:
        txtfile.write("No|Tanggal|Jam|MOR|Kota|SPBU|Nozzle|Dispenser|Produk|Volume|Akumulasi|Revenue|Petugas|Delivery Type|Plat Nomor")
        local_index = 0
        for summary_key in over_list_details:
            summary_item = over_list_details[summary_key]
            for  index, transaction_item in enumerate(over_list_details[summary_key]['transactions']):
                local_index += 1
                cvalue = str(transaction_item['delivery_volume'])

                identifier = summary_key.split('|')
                date = identifier[0]
                plat = identifier[1]
                product = identifier[2]

                aggregate_volume = '{:,.2f}'.format(transaction_item['aggregate_volume'])

                if index == 0:
                    cvalue = aggregate_volume
                txtfile.write("\n" + str(local_index)
                + "|" + date
                + "|" + str(transaction_item['TIME'])
                + "|" + str(summary_item['mor'])
                + "|" + str(transaction_item['city'])
                + "|" + str(transaction_item['site_id'])
                + "|" + str(transaction_item['hose_number'])
                + "|" + str(transaction_item['pump_name'])
                + "|" + product
                + "|" + cvalue
                + "|" + aggregate_volume
                + "|" + str(transaction_item['delivery_value'])
                + "|" + str(transaction_item['attendant_name'])
                + "|" + str(transaction_item['delivery_type'])
                + "|" + plat
                )

print("\n")

if print_transaction_over_two_hundred_liter:
    with open(dir1 + "null_over_limit_transaction.csv", "w") as txtfile:
        txtfile.write("No|Tanggal|Jam|MOR|Kota|SPBU|Nozzle|Dispenser|Produk|Volume|Revenue|Petugas|Delivery Type|Plat Nomor")
        size = 0
        for index, dict_line in enumerate(no_nopol_list):
            if dict_line['delivery_volume'] > treshold:
                txtfile.write("\n" + str(index + 1)
                + "|" + str(dict_line['completed_date'])
                + "|" + str(dict_line['TIME'])
                + "|" + str(dict_line['mor'])
                + "|" + str(dict_line['city'])
                + "|" + str(dict_line['site_id'])
                + "|" + str(dict_line['hose_number'])
                + "|" + str(dict_line['pump_name'])
                + "|" + str(dict_line['product'])
                + "|" + str(dict_line['delivery_volume'])
                + "|" + str(dict_line['delivery_value'])
                + "|" + str(dict_line['attendant_name'])
                + "|" + str(dict_line['delivery_type'])
                + "|" + str(dict_line['vehicle_number'])
                )
                size += 1

        null_over_transaction_string = '{:,.0f}'.format(size)           
        summary += "\n" + 'transaksi tanpa nopol melebihi 200L: ' + null_over_transaction_string

if len(failed_files_list) > 0:
    with open(dir1 + "failed_files.csv", "w") as txtfile:
        txtfile.write("filename")
        for line in failed_files_list:
            txtfile.write("\n" + line)

if len(error_log) > 0:
    with open(dir1 + "error_log.csv", "w") as txtfile:
        txtfile.write("error_log")
        for line in error_log:
            txtfile.write("\n" + line)

with open(dir1 + "summary.txt", "w") as txtfile:
    txtfile.write(summary)

if print_transaction_with_no_nopol:
    with open(dir1 + "no_pol_log.csv", "w") as txtfile:
        txtfile.write("No|Tanggal|Jam|MOR|Kota|SPBU|Nozzle|Dispenser|Produk|Volume|Revenue|Petugas|Delivery Type|Plat Nomor")
        for index, dict_line in enumerate(no_nopol_list):
            txtfile.write("\n" + str(index + 1)
            + "|" + str(dict_line['completed_date'])
            + "|" + str(dict_line['TIME'])
            + "|" + str(dict_line['mor'])
            + "|" + str(dict_line['city'])
            + "|" + str(dict_line['site_id'])
            + "|" + str(dict_line['hose_number'])
            + "|" + str(dict_line['pump_name'])
            + "|" + str(dict_line['product'])
            + "|" + str(dict_line['delivery_volume'])
            + "|" + str(dict_line['delivery_value'])
            + "|" + str(dict_line['attendant_name'])
            + "|" + str(dict_line['delivery_type'])
            + "|" + str(dict_line['vehicle_number'])
            )

if print_transaction_under_one_liter:
    with open(dir1 + "under_transactions.csv", "w") as txtfile:
        txtfile.write("No|Tanggal|Jam|MOR|Kota|SPBU|Nozzle|Dispenser|Produk|Volume|Revenue|Petugas|Delivery Type|Plat Nomor")
        for index, dict_line in enumerate(under_transactions):
            txtfile.write("\n" + str(index + 1)
            + "|" + str(dict_line['completed_date'])
            + "|" + str(dict_line['TIME'])
            + "|" + str(dict_line['mor'])
            + "|" + str(dict_line['city'])
            + "|" + str(dict_line['site_id'])
            + "|" + str(dict_line['hose_number'])
            + "|" + str(dict_line['pump_name'])
            + "|" + str(dict_line['product'])
            + "|" + str(dict_line['delivery_volume'])
            + "|" + str(dict_line['delivery_value'])
            + "|" + str(dict_line['attendant_name'])
            + "|" + str(dict_line['delivery_type'])
            + "|" + str(dict_line['vehicle_number'])
            )

if print_transaction_excluded:
    with open(dir1 + "keywords.csv", "w") as txtfile:
        txtfile.write("Keyword yang dipakai")
        for index, dict_line in enumerate(keywords):
            txtfile.write("\n" + dict_line)

    with open(dir1 + "filtered_by_keywords.csv", "w") as txtfile:
        txtfile.write("No|Tanggal|Jam|MOR|Kota|SPBU|Nozzle|Dispenser|Produk|Volume|Revenue|Petugas|Delivery Type|Plat Nomor")
        for index, dict_line in enumerate(excluded_transactions):
            txtfile.write("\n" + str(index + 1)
            + "|" + str(dict_line['completed_date'])
            + "|" + str(dict_line['TIME'])
            + "|" + str(dict_line['mor'])
            + "|" + str(dict_line['city'])
            + "|" + str(dict_line['site_id'])
            + "|" + str(dict_line['hose_number'])
            + "|" + str(dict_line['pump_name'])
            + "|" + str(dict_line['product'])
            + "|" + str(dict_line['delivery_volume'])
            + "|" + str(dict_line['delivery_value'])
            + "|" + str(dict_line['attendant_name'])
            + "|" + str(dict_line['delivery_type'])
            + "|" + str(dict_line['vehicle_number'])
            )

print(summary)
val = input("Olah data selesai. Laporan telah dibuat. <ENTER> untuk menutup program.")